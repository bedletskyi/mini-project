/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import * as imageService from 'src/services/imageService';
import ExpandedPost from 'src/containers/ExpandedPost';
import Post from 'src/components/Post';
import AddPost from 'src/components/AddPost';
import EditPost from 'src/containers/EditPost';
import EditComment from 'src/containers/EditComment';
import SharedPostLink from 'src/components/SharedPostLink';
import { Checkbox, Loader } from 'semantic-ui-react';
import InfiniteScroll from 'react-infinite-scroller';
import {
  loadPosts,
  loadMorePosts,
  likePost,
  dislikePost,
  toggleExpandedPost,
  toggleEditPost,
  deletePost,
  addPost
} from './actions';

import styles from './styles.module.scss';

const postsFilter = {
  userId: undefined,
  hideMyPosts: false,
  showLikedPosts: false,
  from: 0,
  count: 10
};

const Thread = ({
  userId,
  username,
  loadPosts: load,
  loadMorePosts: loadMore,
  posts = [],
  expandedPost,
  editPost,
  editComment,
  hasMorePosts,
  addPost: createPost,
  likePost: like,
  dislikePost: dislike,
  toggleExpandedPost: toggle,
  toggleEditPost: toggleEdit,
  deletePost: postDelete
}) => {
  const [sharedPostId, setSharedPostId] = useState(undefined);
  const [showOwnPosts, setShowOwnPosts] = useState(false);
  const [hideMyPosts, setHideMyPosts] = useState(false);
  const [showLikedPosts, setShowLikedPosts] = useState(false);

  const togglePost = () => {
    load(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const toggleShowOwnPosts = () => {
    setShowOwnPosts(!showOwnPosts);
    setHideMyPosts(false);
    setShowLikedPosts(false);
    postsFilter.userId = showOwnPosts ? undefined : userId;
    postsFilter.showOwnPosts = !showOwnPosts;
    postsFilter.hideMyPosts = false;
    postsFilter.showLikedPosts = false;
    postsFilter.from = 0;

    togglePost();
  };

  const toggleHideMyPosts = () => {
    setHideMyPosts(!hideMyPosts);
    setShowOwnPosts(false);
    setShowLikedPosts(false);
    postsFilter.userId = hideMyPosts ? undefined : userId;
    postsFilter.hideMyPosts = !hideMyPosts;
    postsFilter.showOwnPosts = false;
    postsFilter.showLikedPosts = false;
    postsFilter.from = 0;

    togglePost();
  };

  const toggleShowLikedPosts = () => {
    setShowLikedPosts(!showLikedPosts);
    setShowOwnPosts(false);
    setHideMyPosts(false);
    postsFilter.userId = showLikedPosts ? undefined : userId;
    postsFilter.showLikedPosts = !showLikedPosts;
    postsFilter.showOwnPosts = false;
    postsFilter.hideMyPosts = false;
    postsFilter.from = 0;

    togglePost();
  };

  const getMorePosts = () => {
    loadMore(postsFilter);
    const { from, count } = postsFilter;
    postsFilter.from = from + count;
  };

  const sharePost = id => {
    setSharedPostId(id);
  };

  const uploadImage = file => imageService.uploadImage(file);

  return (
    <div className={styles.threadContent}>
      <div className={styles.addPostForm}>
        <AddPost addPost={createPost} uploadImage={uploadImage} />
      </div>
      <div className={styles.toolbar}>
        <Checkbox
          className={styles.inlineCheckbox}
          toggle
          label="Show my posts"
          checked={showOwnPosts}
          onChange={toggleShowOwnPosts}
        />
        <Checkbox
          className={styles.inlineCheckbox}
          toggle
          label="Hide my posts"
          checked={hideMyPosts}
          onChange={toggleHideMyPosts}
        />
        <Checkbox
          className={styles.inlineCheckbox}
          toggle
          label="Liked posts"
          checked={showLikedPosts}
          onChange={toggleShowLikedPosts}
        />
      </div>
      <InfiniteScroll
        pageStart={0}
        loadMore={getMorePosts}
        hasMore={hasMorePosts}
        loader={<Loader active inline="centered" key={0} />}
      >
        {posts.map(post => (
          <Post
            post={post}
            likePost={like}
            dislikePost={dislike}
            toggleExpandedPost={toggle}
            sharePost={sharePost}
            editPost={post.user.id === userId ? toggleEdit : undefined}
            deletePost={post.user.id === userId ? postDelete : undefined}
            key={post.id}
          />
        ))}
      </InfiniteScroll>
      {expandedPost && <ExpandedPost sharePost={sharePost} />}
      {editPost && <EditPost uploadImage={uploadImage} />}
      {editComment && <EditComment />}
      {sharedPostId && (
        <SharedPostLink username={username} postId={sharedPostId} close={() => setSharedPostId(undefined)} />
      )}
    </div>
  );
};

Thread.propTypes = {
  posts: PropTypes.arrayOf(PropTypes.object),
  hasMorePosts: PropTypes.bool,
  expandedPost: PropTypes.objectOf(PropTypes.any),
  editPost: PropTypes.objectOf(PropTypes.any),
  editComment: PropTypes.objectOf(PropTypes.any),
  userId: PropTypes.string,
  username: PropTypes.string,
  loadPosts: PropTypes.func.isRequired,
  loadMorePosts: PropTypes.func.isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  toggleEditPost: PropTypes.func.isRequired,
  addPost: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired
};

Thread.defaultProps = {
  posts: [],
  hasMorePosts: true,
  expandedPost: undefined,
  editPost: undefined,
  editComment: undefined,
  userId: undefined,
  username: undefined
};

const mapStateToProps = rootState => ({
  posts: rootState.posts.posts,
  hasMorePosts: rootState.posts.hasMorePosts,
  expandedPost: rootState.posts.expandedPost,
  editPost: rootState.posts.editPost,
  editComment: rootState.posts.editComment,
  userId: rootState.profile.user.id,
  username: rootState.profile.user.username
});

const actions = {
  loadPosts,
  loadMorePosts,
  likePost,
  dislikePost,
  toggleExpandedPost,
  toggleEditPost,
  addPost,
  deletePost
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Thread);
