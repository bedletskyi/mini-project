import {
  SET_ALL_POSTS,
  LOAD_MORE_POSTS,
  ADD_POST,
  UPDATE_POST,
  DELETE_POST,
  SET_EXPANDED_POST,
  SET_EDIT_COMMENT,
  SET_EDIT_POST,
  SET_REACTIONS_LIST
} from './actionTypes';

export default (state = {}, action) => {
  switch (action.type) {
    case SET_ALL_POSTS:
      return {
        ...state,
        posts: action.posts,
        hasMorePosts: Boolean(action.posts.length)
      };
    case LOAD_MORE_POSTS:
      return {
        ...state,
        posts: [...(state.posts || []), ...action.posts],
        hasMorePosts: Boolean(action.posts.length)
      };
    case ADD_POST:
      return {
        ...state,
        posts: [action.post, ...state.posts]
      };
    case UPDATE_POST:
      return {
        ...state,
        posts: [...state.posts.map(post => (post.id === action.post.id ? action.post : post))]
      };
    case DELETE_POST:
      return {
        ...state,
        posts: [...state.posts.filter(post => post.id !== action.id)]
      };
    case SET_EXPANDED_POST:
      return {
        ...state,
        expandedPost: action.post
      };
    case SET_EDIT_POST:
      return {
        ...state,
        editPost: action.post
      };
    case SET_EDIT_COMMENT:
      return {
        ...state,
        editComment: action.comment
      };
    case SET_REACTIONS_LIST:
      return {
        ...state,
        reactionsList: action.reactionsList
      };
    default:
      return state;
  }
};
