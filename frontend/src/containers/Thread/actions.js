import * as postService from 'src/services/postService';
import * as commentService from 'src/services/commentService';
import {
  ADD_POST,
  UPDATE_POST,
  DELETE_POST,
  LOAD_MORE_POSTS,
  SET_ALL_POSTS,
  SET_EXPANDED_POST,
  SET_EDIT_COMMENT,
  SET_EDIT_POST,
  SET_REACTIONS_LIST
} from './actionTypes';

const setPostsAction = posts => ({
  type: SET_ALL_POSTS,
  posts
});

const addMorePostsAction = posts => ({
  type: LOAD_MORE_POSTS,
  posts
});

const addPostAction = post => ({
  type: ADD_POST,
  post
});

const updatePostAction = post => ({
  type: UPDATE_POST,
  post
});

const deletePostAction = id => ({
  type: DELETE_POST,
  id
});

const setExpandedPostAction = post => ({
  type: SET_EXPANDED_POST,
  post
});

const setEditPostAction = post => ({
  type: SET_EDIT_POST,
  post
});

const setEditCommentAction = comment => ({
  type: SET_EDIT_COMMENT,
  comment
});

const setReactionsListAction = reactionsList => ({
  type: SET_REACTIONS_LIST,
  reactionsList
});

export const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  dispatch(setPostsAction(posts));
};

export const loadMorePosts = filter => async (dispatch, getRootState) => {
  const { posts: { posts } } = getRootState();
  const loadedPosts = await postService.getAllPosts(filter);
  const filteredPosts = loadedPosts
    .filter(post => !(posts && posts.some(loadedPost => post.id === loadedPost.id)));
  dispatch(addMorePostsAction(filteredPosts));
};

export const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPostAction(post));
};

export const applyComment = commentId => async (dispatch, getRootState) => {
  const { posts: { expandedPost } } = getRootState();
  const comment = await commentService.getComment(commentId);

  if (!expandedPost || expandedPost?.id !== comment.post.id) {
    return;
  }

  const mapComments = post => {
    if (post.comments.find(comm => comm.id === commentId)) {
      return {
        ...post,
        comments: [...(post.comments || []).map(item => (item.id === commentId ? comment : item))]
      };
    }
    return {
      ...post,
      comments: [...(post.comments || []), comment]
    };
  };

  dispatch(setExpandedPostAction(mapComments(expandedPost)));
};

export const addPost = post => async dispatch => {
  const { id } = await postService.addPost(post);
  const newPost = await postService.getPost(id);
  dispatch(addPostAction(newPost));
};

export const toggleEditPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setEditPostAction(post));
};

export const updatePost = post => async dispatch => {
  const { id } = await postService.updatePost(post);
  const updatedPost = await postService.getPost(id);
  dispatch(updatePostAction(updatedPost));
};

export const deletePost = postId => async dispatch => {
  const response = await postService.deletePost(postId);
  if (!response) {
    return;
  }
  dispatch(deletePostAction(postId));
};

export const toggleExpandedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setExpandedPostAction(post));
};

export const likePost = postId => async (dispatch, getRootState) => {
  const result = await postService.likePost(postId);
  const likeDiff = result?.id ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed
  const dislikeDiff = !result || result?.isNewReaction ? 0 : -1;

  const mapLikes = post => ({
    ...post,
    likeCount: Number(post.likeCount) + likeDiff, // diff is taken from the current closure
    dislikeCount: Number(post.dislikeCount) + dislikeDiff
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapLikes(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapLikes(expandedPost)));
  }
};

export const dislikePost = postId => async (dispatch, getRootState) => {
  const result = await postService.dislikePost(postId);
  const dislikeDiff = result?.id ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed
  const likeDiff = !result || result?.isNewReaction ? 0 : -1;

  const mapDislikes = post => ({
    ...post,
    likeCount: Number(post.likeCount) + likeDiff, // diff is taken from the current closure
    dislikeCount: Number(post.dislikeCount) + dislikeDiff
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapDislikes(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapDislikes(expandedPost)));
  }
};

export const addComment = request => async (dispatch, getRootState) => {
  const result = await commentService.addComment(request);
  const comment = await commentService.getComment(result.id);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) + 1,
    comments: [...(post.comments || []), comment] // comment is taken from the current closure
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== comment.post.id
    ? post
    : mapComments(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === comment.post.id) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

export const toggleEditComment = commentId => async dispatch => {
  const comment = commentId ? await commentService.getComment(commentId) : undefined;
  dispatch(setEditCommentAction(comment));
};

export const updateComment = request => async (dispatch, getRootState) => {
  const { id } = await commentService.updateComment(request);
  const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    comments: [...(post.comments || []).filter(comm => comm.id !== id), comment]
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== comment.post.id
    ? post
    : mapComments(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === comment.post.id) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

export const deleteComment = commentId => async (dispatch, getRootState) => {
  const removedComment = await commentService.getComment(commentId);
  const response = await commentService.deleteComment(commentId);

  if (!response) {
    return;
  }

  const mapComments = post => ({
    ...post,
    comments: [...(post.comments || []).filter(comm => comm.id !== commentId)]
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== removedComment.post.id
    ? post
    : mapComments(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === removedComment.post.id) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

export const likeComment = commentId => async (dispatch, getRootState) => {
  const likedComment = await commentService.getComment(commentId);
  const result = await commentService.likeComment(commentId);
  const likeDiff = result?.id ? 1 : -1; // if ID exists then the comment was liked, otherwise - like was removed
  const dislikeDiff = !result || result?.isNewReaction ? 0 : -1;

  const mapCommentReaction = (item, CommentlikeDiff, CommentdislikeDiff) => ({
    ...item,
    likeCount: Number(item.likeCount) + CommentlikeDiff,
    dislikeCount: Number(item.dislikeCount) + CommentdislikeDiff
  });

  const mapComments = post => ({
    ...post,
    comments: [...(post.comments || []).map(comm => (
      comm.id === commentId ? mapCommentReaction(comm, likeDiff, dislikeDiff) : comm
    ))]
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== likedComment.post.id
    ? post
    : mapComments(post)
  ));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === likedComment.post.id) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

export const dislikeComment = commentId => async (dispatch, getRootState) => {
  const dislikedComment = await commentService.getComment(commentId);
  const result = await commentService.dislikeComment(commentId);
  const dislikeDiff = result?.id ? 1 : -1;
  const likeDiff = !result || result?.isNewReaction ? 0 : -1;

  const mapCommentReaction = (item, CommentlikeDiff, CommentdislikeDiff) => ({
    ...item,
    likeCount: Number(item.likeCount) + CommentlikeDiff,
    dislikeCount: Number(item.dislikeCount) + CommentdislikeDiff
  });

  const mapComments = post => ({
    ...post,
    comments: [...(post.comments || []).map(comm => (
      comm.id === commentId ? mapCommentReaction(comm, likeDiff, dislikeDiff) : comm
    ))]
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== dislikedComment.post.id
    ? post
    : mapComments(post)
  ));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === dislikedComment.post.id) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

export const getReactionsList = (id, type) => async dispatch => {
  const getReactions = type === 'post' ? postService.getPostReactions : commentService.getCommentReactions;

  const reactionsList = id ? await getReactions(id) : undefined;
  dispatch(setReactionsListAction(reactionsList));
};
