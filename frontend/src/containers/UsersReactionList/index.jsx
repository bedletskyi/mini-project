import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { getUserImgLink } from 'src/helpers/imageHelper';
import { getReactionsList } from 'src/containers/Thread/actions';
import { Image, Popup, List, Icon, Label, Placeholder } from 'semantic-ui-react';

const UsersReactionList = ({
  reactionCount,
  reactionsList,
  postReaction,
  postId,
  type,
  getReactionsList: getReactions,
  isLike: like
}) => {
  const trigger = () => {
    if (type === 'post') {
      return (
        <Label basic size="small" as="a" style={{ border: 'none' }} onClick={() => postReaction(postId)}>
          <Icon name={like ? 'thumbs up' : 'thumbs down'} />
          {reactionCount}
        </Label>
      );
    }
    return (
      <span onClick={() => postReaction(postId)}>
        <Icon name={like ? 'thumbs up' : 'thumbs down'} />
        {reactionCount}
      </span>
    );
  };

  return (
    <>
      {reactionCount > 0
        ? (
          <Popup
            style={{ filter: 'none' }}
            onClose={() => getReactions()}
            onOpen={() => getReactions(postId, type)}
            popperDependencies={[!!reactionsList]}
            trigger={trigger()}
            hoverable
            wide
          >
            {
              reactionsList === undefined ? (
                <Placeholder style={{ minWidth: '100px' }}>
                  <Placeholder.Header>
                    <Placeholder.Line />
                    <Placeholder.Line />
                  </Placeholder.Header>
                  <Placeholder.Paragraph>
                    <Placeholder.Line length="medium" />
                    <Placeholder.Line length="short" />
                  </Placeholder.Paragraph>
                </Placeholder>
              )
                : (
                  <List verticalAlign="middle" size="small">
                    {reactionsList.filter(({ isLike }) => isLike === like).map(({ id, user }) => (
                      <List.Item key={id}>
                        <Image avatar src={getUserImgLink(user.image)} />
                        <List.Content>
                          <List.Header>{user.username}</List.Header>
                        </List.Content>
                      </List.Item>
                    ))}
                  </List>
                )
            }
          </Popup>
        )
        : trigger()}
    </>
  );
};

UsersReactionList.propTypes = {
  reactionCount: PropTypes.number.isRequired,
  isLike: PropTypes.bool.isRequired,
  type: PropTypes.oneOf(['post', 'comment']).isRequired,
  postId: PropTypes.string.isRequired,
  postReaction: PropTypes.func.isRequired,
  getReactionsList: PropTypes.func.isRequired,
  reactionsList: PropTypes.arrayOf(PropTypes.object)
};

UsersReactionList.defaultProps = {
  reactionsList: undefined
};

const mapStateToProps = rootState => ({
  reactionsList: rootState.posts.reactionsList
});

const actions = {
  getReactionsList
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UsersReactionList);
