import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Modal } from 'semantic-ui-react';
import { updatePost, toggleEditPost } from 'src/containers/Thread/actions';
import Spinner from 'src/components/Spinner';
import EditPostForm from 'src/components/EditPostForm';

const EditPost = ({
  post,
  toggleEditPost: toggle,
  updatePost: update,
  uploadImage
}) => (
  <Modal dimmer="blurring" centered={false} open onClose={() => toggle()}>
    {post
      ? (
        <Modal.Content>
          <EditPostForm post={post} update={update} uploadImage={uploadImage} close={toggle} />
        </Modal.Content>
      )
      : <Spinner />}
  </Modal>
);

EditPost.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  toggleEditPost: PropTypes.func.isRequired,
  updatePost: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired
};

const mapStateToProps = rootState => ({
  post: rootState.posts.editPost
});

const actions = { updatePost, toggleEditPost };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditPost);
