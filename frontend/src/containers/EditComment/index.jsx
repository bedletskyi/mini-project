import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Modal } from 'semantic-ui-react';
import { updateComment, toggleEditComment } from 'src/containers/Thread/actions';
import Spinner from 'src/components/Spinner';
import EditCommentForm from 'src/components/EditCommentForm';

const EditComment = ({
  comment,
  toggleEditComment: toggle,
  updateComment: update
}) => (
  <Modal dimmer="blurring" centered={false} open onClose={() => toggle()}>
    {comment
      ? (
        <Modal.Content>
          <EditCommentForm comment={comment} update={update} close={toggle} />
        </Modal.Content>
      )
      : <Spinner />}
  </Modal>
);

EditComment.propTypes = {
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  toggleEditComment: PropTypes.func.isRequired,
  updateComment: PropTypes.func.isRequired
};

const mapStateToProps = rootState => ({
  comment: rootState.posts.editComment
});

const actions = { updateComment, toggleEditComment };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditComment);
