import * as authService from 'src/services/authService';
import validator from 'validator';
import {
  SET_USER, SET_IS_VALID_USERNAME,
  SET_IS_VALID_RESET_PASSWORD_TOKEN,
  SET_IS_SAVED_PASSWORD
} from './actionTypes';

const setToken = token => localStorage.setItem('token', token);

const setUser = user => async dispatch => dispatch({
  type: SET_USER,
  user
});

const setIsValidUsername = result => async dispatch => dispatch({
  type: SET_IS_VALID_USERNAME,
  result
});

const setIsValidResetPasswordToken = result => async dispatch => dispatch({
  type: SET_IS_VALID_RESET_PASSWORD_TOKEN,
  result
});

const setIsSavedPassword = result => async dispatch => dispatch({
  type: SET_IS_SAVED_PASSWORD,
  result
});

const setAuthData = (user = null, token = '') => (dispatch, getRootState) => {
  setToken(token); // token should be set first before user
  setUser(user)(dispatch, getRootState);
};

const handleAuthResponse = authResponsePromise => async (dispatch, getRootState) => {
  const { user, token } = await authResponsePromise;
  setAuthData(user, token)(dispatch, getRootState);
};

export const login = request => handleAuthResponse(authService.login(request));

export const register = request => handleAuthResponse(authService.registration(request));

export const logout = () => setAuthData();

export const validateResetPasswordToken = resetToken => async (dispatch, getRootState) => {
  try {
    const result = await authService.validateToken(resetToken);
    setIsValidResetPasswordToken({ isValid: true, token: result.token })(dispatch, getRootState);
  } catch {
    setIsValidResetPasswordToken({ isValid: false, token: resetToken })(dispatch, getRootState);
  }
};

export const saveNewPassword = request => async (dispatch, getRootState) => {
  try {
    await authService.saveNewPassword(request);
    setIsSavedPassword(true)(dispatch, getRootState);
  } catch {
    setIsSavedPassword(false)(dispatch, getRootState);
  }
};

export const loadCurrentUser = () => async (dispatch, getRootState) => {
  const user = await authService.getCurrentUser();
  setUser(user)(dispatch, getRootState);
};

export const updateCurrentUser = user => async (dispatch, getRootState) => {
  const updatedUser = await authService.updateCurrentUser(user);
  setUser(updatedUser)(dispatch, getRootState);
};

export const validateUsername = (newUsername, oldUsername) => async (dispatch, getRootState) => {
  if (validator.isEmpty(newUsername)) {
    setIsValidUsername(false)(dispatch, getRootState);
    return;
  }
  if (newUsername === oldUsername) {
    setIsValidUsername(true)(dispatch, getRootState);
    return;
  }

  const usersList = await authService.getAllUsers();
  if (usersList.findIndex(user => user.username === newUsername) === -1) {
    setIsValidUsername(true)(dispatch, getRootState);
  } else {
    setIsValidUsername(false)(dispatch, getRootState);
  }
};
