import {
  SET_USER,
  SET_IS_VALID_USERNAME,
  SET_IS_VALID_RESET_PASSWORD_TOKEN,
  SET_IS_SAVED_PASSWORD
} from './actionTypes';

export default (state = {}, action) => {
  switch (action.type) {
    case SET_USER:
      return {
        ...state,
        user: action.user,
        isAuthorized: Boolean(action.user?.id),
        isLoading: false
      };
    case SET_IS_VALID_USERNAME:
      return {
        ...state,
        isValidUsername: action.result
      };
    case SET_IS_VALID_RESET_PASSWORD_TOKEN:
      return {
        ...state,
        isValidResetToken: action.result.isValid,
        resetToken: action.result.token
      };
    case SET_IS_SAVED_PASSWORD:
      return {
        ...state,
        isSavedPassword: action.result
      };
    default:
      return state;
  }
};
