export const SET_USER = 'PROFILE_ACTION:SET_USER';
export const SET_IS_VALID_USERNAME = 'PROFILE_ACTION:SET_IS_VALID_USERNAME';
export const SET_IS_VALID_RESET_PASSWORD_TOKEN = 'PROFILE_ACTION:SET_IS_VALID_RESET_PASSWORD_TOKEN';
export const SET_IS_SAVED_PASSWORD = 'PROFILE_ACTION:SET_IS_SAVED_PASSWORD';
