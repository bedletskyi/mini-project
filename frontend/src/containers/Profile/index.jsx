import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getUserImgLink } from 'src/helpers/imageHelper';
import * as imageService from 'src/services/imageService';
import CroppedImage from 'src/components/CroppedImage';
import {
  Grid,
  Image,
  Input,
  Button,
  Icon
} from 'semantic-ui-react';
import { validateUsername, updateCurrentUser } from './actions';

const Profile = ({
  user,
  isValidUsername,
  validateUsername: validate,
  updateCurrentUser: update
}) => {
  const [isUploading, setIsUploading] = useState(false);
  const [edit, setEdit] = useState(false);
  const [uploadedImage, setUploadedImage] = useState(undefined);
  const [currentUser, setUser] = useState(user);

  useEffect(() => {
    setUser(user);
  }, [user]);

  const handleUploadFile = e => {
    if (e.target.files && e.target.files.length > 0) {
      const reader = new FileReader();
      reader.addEventListener('load', () => setUploadedImage(reader.result));
      reader.readAsDataURL(e.target.files[0]);
    }
  };

  const closeModal = () => {
    setUploadedImage(undefined);
  };

  const saveCropp = async img => {
    const image = await imageService.uploadImage(img);
    setUser({ ...currentUser, image });
    closeModal();
  };

  const saveProfile = async () => {
    if (!isValidUsername) return;
    setIsUploading(true);
    const { username, image, status } = currentUser;
    await update({ ...user, username, image, status });
    setIsUploading(false);
    setEdit(false);
  };

  const cancelEdit = () => {
    setUser(user);
    setEdit(false);
  };

  return (
    <Grid container textAlign="center" style={{ paddingTop: 30 }}>
      <Grid.Column>
        <div style={{ position: 'relative', display: 'inline-block' }}>
          <Image
            centered
            src={getUserImgLink(currentUser.image)}
            size="medium"
            circular
          />
          {
            edit && (
              <div style={{ position: 'absolute', right: '0px', bottom: '0px' }}>
                <Button circular icon as="label">
                  <Icon name="photo" />
                  <input name="image" type="file" onChange={handleUploadFile} hidden />
                </Button>
              </div>
            )
          }
        </div>
        <br />
        <br />
        <Input
          icon="user"
          iconPosition="left"
          placeholder="Username"
          type="text"
          disabled={!edit}
          error={!isValidUsername}
          onChange={ev => setUser({ ...currentUser, username: ev.target.value })}
          onBlur={() => validate(currentUser.username, user.username)}
          value={currentUser.username}
        />
        <br />
        <br />
        <Input
          icon="at"
          iconPosition="left"
          placeholder="Email"
          type="email"
          disabled
          value={currentUser.email}
        />
        <br />
        <br />
        <Input
          icon="comment alternate"
          iconPosition="left"
          placeholder="Tell us more"
          type="text"
          disabled={!edit}
          onChange={ev => setUser({ ...currentUser, status: ev.target.value })}
          value={currentUser.status}
        />
        <br />
        <br />
        {
          edit ? (
            <div>
              <Button.Group>
                <Button
                  positive
                  as="label"
                  onClick={saveProfile}
                  loading={isUploading}
                  disabled={isUploading}
                >
                  Save
                </Button>
                <Button.Or />
                <Button
                  as="label"
                  onClick={cancelEdit}
                >
                  Cancel
                </Button>
              </Button.Group>
            </div>
          ) : (
            <Button
              color="blue"
              as="label"
              onClick={() => setEdit(true)}
            >
              Edit
            </Button>
          )
        }
      </Grid.Column>
      {uploadedImage && (
        <CroppedImage image={uploadedImage} save={saveCropp} close={closeModal} />
      )}
    </Grid>
  );
};

Profile.propTypes = {
  user: PropTypes.objectOf(PropTypes.any),
  updateCurrentUser: PropTypes.func.isRequired,
  validateUsername: PropTypes.func.isRequired,
  isValidUsername: PropTypes.bool
};

Profile.defaultProps = {
  user: {},
  isValidUsername: true
};

const mapStateToProps = rootState => ({
  user: rootState.profile.user,
  isValidUsername: rootState.profile.isValidUsername
});

const actions = {
  validateUsername,
  updateCurrentUser
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Profile);
