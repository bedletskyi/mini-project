import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { useRouteMatch, NavLink } from 'react-router-dom';
import Logo from 'src/components/Logo';
import CreateNewPasswordForm from 'src/components/CreateNewPasswordForm';
import { validateResetPasswordToken, saveNewPassword } from 'src/containers/Profile/actions';
import {
  Grid,
  Header,
  Button
} from 'semantic-ui-react';

const CreateNewPassword = ({
  isValidResetToken,
  resetToken,
  isSavedPassword,
  validateResetPasswordToken: validateToken,
  saveNewPassword: savePassword
}) => {
  const match = useRouteMatch('/password/:token');

  useEffect(() => {
    validateToken(match.params.token);
  });

  return (
    <Grid textAlign="center" verticalAlign="middle" className="fill">
      <Grid.Column style={{ maxWidth: 450 }}>
        <Logo />
        {isSavedPassword
          ? (
            <>
              <p>You password has been saved.</p>
              <NavLink exact to="/login">
                <Button primary>Sign back in</Button>
              </NavLink>
            </>
          ) : (
            <>
              <Header as="h2" color="teal" textAlign="center">
                Update your password
              </Header>
              {isValidResetToken
                ? <CreateNewPasswordForm send={savePassword} token={resetToken} isSavedPassword={isSavedPassword} />
                : (
                  <>
                    <p>Oops, something went wrong!!!</p>
                    <NavLink exact to="/login">
                      <Button primary>Sign back in</Button>
                    </NavLink>
                  </>
                )}
            </>
          )}
      </Grid.Column>
    </Grid>
  );
};

CreateNewPassword.propTypes = {
  isValidResetToken: PropTypes.bool,
  isSavedPassword: PropTypes.bool,
  resetToken: PropTypes.string,
  validateResetPasswordToken: PropTypes.func.isRequired,
  saveNewPassword: PropTypes.func.isRequired
};

CreateNewPassword.defaultProps = {
  isValidResetToken: true,
  isSavedPassword: false,
  resetToken: ''
};

const mapStateToProps = rootState => ({
  isValidResetToken: rootState.profile.isValidResetToken,
  resetToken: rootState.profile.resetToken,
  isSavedPassword: rootState.profile.isSavedPassword
});

const actions = { validateResetPasswordToken, saveNewPassword };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CreateNewPassword);
