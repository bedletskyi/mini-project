import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { getUserImgLink } from 'src/helpers/imageHelper';
import { updateCurrentUser } from 'src/containers/Profile/actions';
import { Header as HeaderUI, Image, Grid, Icon, Button, Input } from 'semantic-ui-react';

import styles from './styles.module.scss';

const Header = ({ user, logout, updateCurrentUser: update }) => {
  const [editStatus, setEditStatus] = useState(false);
  const [status, setStatus] = useState(user.status);

  useEffect(() => {
    setStatus(user.status);
  }, [user.status]);

  const handleSaveStatus = async () => {
    await update({ ...user, status });
    setEditStatus(false);
  };

  return (
    <div className={styles.headerWrp}>
      <Grid centered container columns="2">
        <Grid.Column>
          {user && (
            <HeaderUI>
              <Image circular src={getUserImgLink(user.image)} />
              <HeaderUI.Content>
                <NavLink exact to="/">
                  {user.username}
                </NavLink>
                {user.status && (
                  !editStatus
                    ? (
                      <HeaderUI.Subheader>
                        {user.status}
                        {' '}
                        <Icon
                          name="edit outline"
                          link
                          onClick={() => setEditStatus(true)}
                        />
                      </HeaderUI.Subheader>
                    ) : (
                      <HeaderUI.Subheader>
                        <Input
                          type="text"
                          focus
                          transparent
                          onChange={ev => setStatus(ev.target.value)}
                          value={status}
                        />
                        {' '}
                        <Icon name="save outline" link onClick={handleSaveStatus} />
                        <Icon
                          name="times circle outline"
                          link
                          onClick={() => {
                            setEditStatus(false);
                            setStatus(user.stetus);
                          }}
                        />
                      </HeaderUI.Subheader>
                    )
                )}
              </HeaderUI.Content>
            </HeaderUI>
          )}
        </Grid.Column>
        <Grid.Column textAlign="right">
          <NavLink exact activeClassName="active" to="/profile" className={styles.menuBtn}>
            <Icon name="user circle" size="large" />
          </NavLink>
          <Button basic icon type="button" className={`${styles.menuBtn} ${styles.logoutBtn}`} onClick={logout}>
            <Icon name="log out" size="large" />
          </Button>
        </Grid.Column>
      </Grid>
    </div>
  );
};

Header.propTypes = {
  logout: PropTypes.func.isRequired,
  user: PropTypes.objectOf(PropTypes.any).isRequired,
  updateCurrentUser: PropTypes.func.isRequired
};

const mapStateToProps = rootState => ({
  user: rootState.profile.user
});

const actions = {
  updateCurrentUser
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Header);
