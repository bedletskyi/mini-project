import callWebApi from 'src/helpers/webApiHelper';

export const login = async request => {
  const response = await callWebApi({
    endpoint: '/api/auth/login',
    type: 'POST',
    request
  });
  return response.json();
};

export const registration = async request => {
  const response = await callWebApi({
    endpoint: '/api/auth/register',
    type: 'POST',
    request
  });
  return response.json();
};

export const sendResetEmail = async request => {
  const response = await callWebApi({
    endpoint: '/api/auth/password/reset',
    type: 'POST',
    request
  });
  return response.json();
};

export const validateToken = async token => {
  const response = await callWebApi({
    endpoint: `/api/auth/password/${token}`,
    type: 'GET'
  });
  return response.json();
};

export const saveNewPassword = async request => {
  const response = await callWebApi({
    endpoint: '/api/auth/password/new',
    type: 'PUT',
    request
  });
  return response.json();
};

export const getCurrentUser = async () => {
  try {
    const response = await callWebApi({
      endpoint: '/api/user',
      type: 'GET'
    });
    return response.json();
  } catch (e) {
    return null;
  }
};

export const getAllUsers = async () => {
  const response = await callWebApi({
    endpoint: '/api/user/all',
    type: 'GET'
  });
  return response.json();
};

export const updateCurrentUser = async request => {
  const response = await callWebApi({
    endpoint: '/api/user',
    type: 'PUT',
    request
  });
  return response.json();
};
