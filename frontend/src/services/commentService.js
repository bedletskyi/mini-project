import callWebApi from 'src/helpers/webApiHelper';

export const addComment = async request => {
  const response = await callWebApi({
    endpoint: '/api/comments',
    type: 'POST',
    request
  });
  return response.json();
};

export const getComment = async id => {
  const response = await callWebApi({
    endpoint: `/api/comments/${id}`,
    type: 'GET'
  });
  return response.json();
};

export const updateComment = async request => {
  const response = await callWebApi({
    endpoint: `/api/comments/${request.id}`,
    type: 'PUT',
    request
  });
  return response.json();
};

export const deleteComment = async id => {
  const response = await callWebApi({
    endpoint: `/api/comments/${id}`,
    type: 'DELETE'
  });
  return response.json();
};

export const likeComment = async commentId => {
  const response = await callWebApi({
    endpoint: '/api/commentreaction',
    type: 'PUT',
    request: {
      commentId,
      isLike: true
    }
  });

  return response.json();
};

export const dislikeComment = async commentId => {
  const response = await callWebApi({
    endpoint: '/api/commentreaction',
    type: 'PUT',
    request: {
      commentId,
      isLike: false
    }
  });

  return response.json();
};

export const getCommentReactions = async commentId => {
  const response = await callWebApi({
    endpoint: `/api/commentreaction/${commentId}`,
    type: 'GET'
  });
  return response.json();
};
