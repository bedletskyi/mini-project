import React, { useState, useRef } from 'react';
import validator from 'validator';
import PropTypes from 'prop-types';
import { Modal, Input, Icon } from 'semantic-ui-react';
import { sharePostByEmail } from 'src/services/postService';

import styles from './styles.module.scss';

const SharedPostLink = ({ username, postId, close }) => {
  const [copied, setCopied] = useState(false);
  const [email, setEmail] = useState('');
  const [isEmailValid, setIsEmailValid] = useState(true);
  const [isLoading, setIsLoading] = useState(false);
  const link = `${window.location.origin}/share/${postId}`;

  let input = useRef();

  const emailChanged = data => {
    setEmail(data);
    setIsEmailValid(true);
  };

  const validate = () => {
    const isValid = validator.isEmail(email);
    setIsEmailValid(isValid);
    return isValid;
  };

  const handleShareClick = async () => {
    if (!validate()) {
      return;
    }
    const request = {
      username,
      postLink: link,
      recipientEmail: email
    };
    setIsLoading(true);
    try {
      await sharePostByEmail(request);
    } catch {
      setIsLoading(false);
      setEmail('');
    }
    setIsLoading(false);
    setEmail('');
  };

  const copyToClipboard = e => {
    input.select();
    document.execCommand('copy');
    e.target.focus();
    setCopied(true);
  };

  return (
    <Modal open onClose={close}>
      <Modal.Header className={styles.header}>
        <span>Share Post</span>
        {copied && (
          <span>
            <Icon color="green" name="copy" />
            Copied
          </span>
        )}
      </Modal.Header>
      <Modal.Content>
        <Input
          fluid
          action={{
            color: 'teal',
            labelPosition: 'right',
            icon: 'copy',
            content: 'Copy',
            onClick: copyToClipboard
          }}
          value={link}
          ref={ref => { input = ref; }}
        />
        <br />
        <br />
        <Input
          fluid
          action={{
            color: 'blue',
            labelPosition: 'right',
            icon: 'share',
            content: 'Share',
            loading: isLoading,
            onClick: handleShareClick
          }}
          error={!isEmailValid}
          value={email}
          onBlur={validate}
          onChange={ev => emailChanged(ev.target.value)}
        />
      </Modal.Content>
    </Modal>
  );
};

SharedPostLink.propTypes = {
  postId: PropTypes.string.isRequired,
  close: PropTypes.func.isRequired,
  username: PropTypes.string.isRequired
};

export default SharedPostLink;
