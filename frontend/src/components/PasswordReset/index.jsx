import React from 'react';
import { NavLink } from 'react-router-dom';
import Logo from 'src/components/Logo';
import PasswordResetForm from 'src/components/PasswordResetForm';
import * as authService from 'src/services/authService';
import {
  Grid,
  Header,
  Message
} from 'semantic-ui-react';

const PasswordReset = () => (
  <Grid textAlign="center" verticalAlign="middle" className="fill">
    <Grid.Column style={{ maxWidth: 450 }}>
      <Logo />
      <Header as="h2" color="teal" textAlign="center">
        Reset password
      </Header>
      <PasswordResetForm send={authService.sendResetEmail} />
      <Message>
        <NavLink exact to="/login">
          Sign back In
        </NavLink>
      </Message>
    </Grid.Column>
  </Grid>
);

export default PasswordReset;
