import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Stomp } from '@stomp/stompjs';
import SockJS from 'sockjs-client';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import 'react-notifications/lib/notifications.css';

const Notifications = ({ user, applyPost, applyComment }) => {
  const [stompClient] = useState(Stomp.over(new SockJS('/ws')));

  useEffect(() => {
    if (!user) {
      return undefined;
    }

    stompClient.debug = () => { };
    stompClient.connect({}, () => {
      console.log('connected');

      const { id } = user;

      stompClient.subscribe('/topic/like', message => {
        const { isLike, authorPostId, userId } = JSON.parse(message.body);
        if (isLike && authorPostId === id && userId !== id) {
          NotificationManager.info('Your post was liked!');
        }
      });

      stompClient.subscribe('/topic/commentlike', message => {
        const { isLike, authorId, userId } = JSON.parse(message.body);
        if (isLike && authorId === id && userId !== id) {
          NotificationManager.info('Your comment was liked!');
        }
      });

      stompClient.subscribe('/topic/new_comment', message => {
        const { post, user: commentAuthor, id: commentId } = JSON.parse(message.body);
        if (post.userId === id && commentAuthor.id !== id) {
          NotificationManager.info('Your post was commented');
        }
        if (commentAuthor.id !== id) {
          applyComment(commentId);
        }
      });

      stompClient.subscribe('/topic/update_comment', message => {
        const { user: commentAuthor, id: commentId } = JSON.parse(message.body);
        if (commentAuthor.id !== id) {
          applyComment(commentId);
        }
      });

      stompClient.subscribe('/topic/new_post', message => {
        const post = JSON.parse(message.body);
        if (post.userId !== id) {
          applyPost(post.id);
        }
      });
    });

    return () => {
      stompClient.disconnect();
    };
  });

  return <NotificationContainer />;
};

Notifications.defaultProps = {
  user: undefined
};

Notifications.propTypes = {
  user: PropTypes.objectOf(PropTypes.any),
  applyPost: PropTypes.func.isRequired,
  applyComment: PropTypes.func.isRequired
};

export default Notifications;
