import React, { useState } from 'react';
import PropTypes from 'prop-types';
import validator from 'validator';
import {
  Form,
  Button,
  Segment
} from 'semantic-ui-react';

const PasswordResetForm = ({ send, token, isSavedPassword }) => {
  const [passwordFirst, setPasswordFirst] = useState('');
  const [passwordSecond, setPasswordSecond] = useState('');
  const [isPasswordValid, setIsPasswordValid] = useState(true);
  const [isLoading, setIsLoading] = useState(false);

  const validatePassword = () => {
    const isIdentical = passwordFirst === passwordSecond;
    const isLengthValid = validator.isLength(passwordFirst, [6, 30]);
    const isValid = isIdentical && isLengthValid;
    setIsPasswordValid(isValid);
    return isValid;
  };

  const handleSaveClick = async () => {
    if (!validatePassword() || isSavedPassword) {
      return;
    }
    setIsLoading(true);
    const request = { password: passwordFirst, token };
    try {
      await send(request);
    } catch {
      setIsLoading(false);
    }
  };

  return (
    <Form name="resettingForm" size="large" onSubmit={handleSaveClick}>
      <Segment>
        <Form.Input
          fluid
          icon="lock"
          iconPosition="left"
          placeholder="Password"
          type="password"
          error={!isPasswordValid}
          onChange={ev => setPasswordFirst(ev.target.value)}
          onBlur={validatePassword}
        />
        <Form.Input
          fluid
          icon="lock"
          iconPosition="left"
          placeholder="Password"
          type="password"
          error={!isPasswordValid}
          onChange={ev => setPasswordSecond(ev.target.value)}
          onBlur={validatePassword}
        />
        <Button
          type="submit"
          color={isPasswordValid ? 'blue' : 'google plus'}
          fluid
          size="large"
          loading={isLoading}
        >
          Save
        </Button>
      </Segment>
    </Form>
  );
};

PasswordResetForm.propTypes = {
  send: PropTypes.func.isRequired,
  token: PropTypes.string.isRequired,
  isSavedPassword: PropTypes.bool.isRequired
};

export default PasswordResetForm;
