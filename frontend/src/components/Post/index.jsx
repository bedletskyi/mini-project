import React from 'react';
import PropTypes from 'prop-types';
import { Card, Image, Label, Icon, Dropdown } from 'semantic-ui-react';
import moment from 'moment';
import UsersReactionList from 'src/containers/UsersReactionList';

import styles from './styles.module.scss';

const Post = ({ post, likePost, dislikePost, toggleExpandedPost, sharePost, editPost, deletePost }) => {
  const {
    id,
    image,
    body,
    user,
    likeCount,
    dislikeCount,
    commentCount,
    createdAt
  } = post;
  const date = moment(createdAt).fromNow();
  return (
    <Card style={{ width: '100%' }}>
      {image && <Image src={image.link} wrapped ui={false} />}
      <Card.Content>
        <Card.Meta>
          <span className="date">
            posted by
            {' '}
            {user.username}
            {' - '}
            {date}
          </span>
        </Card.Meta>
        <Card.Description>
          {body}
        </Card.Description>
      </Card.Content>
      <Card.Content extra>
        <UsersReactionList
          reactionCount={likeCount}
          isLike
          type="post"
          postId={id}
          postReaction={likePost}
        />
        <UsersReactionList
          reactionCount={dislikeCount}
          isLike={false}
          type="post"
          postReaction={dislikePost}
          postId={id}
        />
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => toggleExpandedPost(id)}>
          <Icon name="comment" />
          {commentCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => sharePost(id)}>
          <Icon name="share alternate" />
        </Label>
        {editPost && deletePost && (
          <Dropdown text="Post" className={`${styles.toolbarBtn} ${styles.right}`}>
            <Dropdown.Menu>
              <Dropdown.Item icon="edit" text="Edit" onClick={() => editPost(id)} />
              <Dropdown.Item icon="trash" text="Delete" onClick={() => deletePost(id)} />
            </Dropdown.Menu>
          </Dropdown>
        )}
      </Card.Content>
    </Card>
  );
};

Post.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  editPost: PropTypes.func,
  deletePost: PropTypes.func
};

Post.defaultProps = {
  editPost: undefined,
  deletePost: undefined
};

export default Post;
