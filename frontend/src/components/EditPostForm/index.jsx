import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Form, Button, Icon, Image, Segment } from 'semantic-ui-react';

import styles from './styles.module.scss';

const EditPostForm = ({
  update,
  uploadImage,
  close,
  post
}) => {
  const [body, setBody] = useState(post.body);
  const [image, setImage] = useState({ imageId: post.image?.id, imageLink: post.image?.link });
  const [isUploading, setIsUploading] = useState(false);

  const handleUpdatePost = async () => {
    if (!body) {
      return;
    }
    await update({ ...post, imageId: image?.imageId, body });
    close();
  };

  const handleUploadFile = async ({ target }) => {
    setIsUploading(true);
    try {
      const { id: imageId, link: imageLink } = await uploadImage(target.files[0]);
      setImage({ imageId, imageLink });
    } finally {
      // TODO: show error
      setIsUploading(false);
    }
  };

  return (
    <Segment>
      <Form onSubmit={handleUpdatePost}>
        <Form.TextArea
          name="body"
          value={body}
          placeholder="What is the news?"
          style={{ minHeight: 150 }}
          onChange={ev => setBody(ev.target.value)}
        />
        {image?.imageLink && (
          <div className={styles.imageWrapper}>
            <Image className={styles.image} src={image?.imageLink} alt="post" />
          </div>
        )}
        <Button color="teal" icon labelPosition="left" as="label" loading={isUploading} disabled={isUploading}>
          <Icon name="image" />
          Attach image
          <input name="image" type="file" onChange={handleUploadFile} hidden />
        </Button>
        <Button floated="right" color="blue" type="submit">Update</Button>
      </Form>
    </Segment>
  );
};

EditPostForm.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  update: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired,
  close: PropTypes.func.isRequired
};

export default EditPostForm;
