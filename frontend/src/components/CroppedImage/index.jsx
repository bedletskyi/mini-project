import React, { useState, useCallback, useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import ReactCrop from 'react-image-crop';
import { getResizedImage } from 'src/helpers/imageHelper';
import Spinner from 'src/components/Spinner';
import { Modal, Button } from 'semantic-ui-react';

import 'react-image-crop/lib/ReactCrop.scss';

const CroppedImage = ({ image, save, close }) => {
  const imgRef = useRef(null);
  const previewCanvasRef = useRef(null);
  const [completedCrop, setCompletedCrop] = useState(null);
  const maxSize = { maxWidth: 200, maxHeight: 200 };
  const [crop, setCrop] = useState({
    unit: 'px',
    width: 100,
    height: 100,
    aspect: 1 / 1
  });

  const onLoad = useCallback(img => {
    imgRef.current = img;
  }, []);

  const onChange = cropField => {
    const width = cropField.width > maxSize.maxWidth ? maxSize.maxWidth : cropField.width;
    const height = cropField.height > maxSize.maxHeight ? maxSize.maxHeight : cropField.height;

    setCrop({ ...cropField, width, height });
  };

  const saveImage = async () => {
    const img = await getResizedImage(previewCanvasRef.current, completedCrop);
    save(img);
  };

  useEffect(() => {
    if (!completedCrop || !previewCanvasRef.current || !imgRef.current) {
      return;
    }

    const img = imgRef.current;
    const canvas = previewCanvasRef.current;
    const cropping = completedCrop;
    const dpr = window.devicePixelRatio || 1;

    const scaleX = img.naturalWidth / img.width;
    const scaleY = img.naturalHeight / img.height;
    const ctx = canvas.getContext('2d');

    canvas.width = cropping.width * dpr;
    canvas.height = cropping.height * dpr;

    ctx.drawImage(
      img,
      cropping.x * scaleX,
      cropping.y * scaleY,
      cropping.width * scaleX,
      cropping.height * scaleY,
      0,
      0,
      cropping.width * dpr,
      cropping.height * dpr
    );
  }, [completedCrop]);

  return (
    <Modal dimmer="blurring" centered={false} open onClose={close}>
      {image
        ? (
          <Modal.Content>
            <ReactCrop
              src={image}
              crop={crop}
              ruleOfThirds
              onImageLoaded={onLoad}
              onComplete={c => setCompletedCrop(c)}
              onChange={c => onChange(c)}
            />
            {previewCanvasRef && (
              <>
                <br />
                <canvas
                  ref={previewCanvasRef}
                  style={{
                    width: completedCrop?.width ?? 0,
                    height: completedCrop?.height ?? 0,
                    marginLeft: 'auto',
                    marginRight: 'auto'
                  }}
                />
                <br />
                <Button.Group>
                  <Button
                    positive
                    as="label"
                    onClick={saveImage}
                  >
                    Ok
                  </Button>
                  <Button.Or />
                  <Button
                    as="label"
                    onClick={close}
                  >
                    Cancel
                  </Button>
                </Button.Group>
              </>
            )}
          </Modal.Content>
        ) : (
          <Spinner />
        )}
    </Modal>
  );
};

CroppedImage.propTypes = {
  image: PropTypes.string.isRequired,
  save: PropTypes.func.isRequired,
  close: PropTypes.func.isRequired
};

export default CroppedImage;
