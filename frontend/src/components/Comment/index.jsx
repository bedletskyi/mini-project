import React from 'react';
import PropTypes from 'prop-types';
import { Comment as CommentUI, Icon } from 'semantic-ui-react';
import moment from 'moment';
import { getUserImgLink } from 'src/helpers/imageHelper';
import UsersReactionList from 'src/containers/UsersReactionList';

import styles from './styles.module.scss';

const Comment = ({
  comment: { body, createdAt, user, id, dislikeCount, likeCount },
  toggleEditComment,
  deleteComment,
  likeComment,
  dislikeComment
}) => (
  <CommentUI className={styles.comment}>
    <CommentUI.Avatar src={getUserImgLink(user.image)} />
    <CommentUI.Content>
      <CommentUI.Author as="a">
        {user.username}
      </CommentUI.Author>
      <CommentUI.Metadata>
        {moment(createdAt).fromNow()}
      </CommentUI.Metadata>
      {user.status !== null && (
        <CommentUI.Metadata style={{ marginLeft: '0em', display: 'block' }}>
          {user.status}
        </CommentUI.Metadata>
      )}
      <CommentUI.Text>
        {body}
      </CommentUI.Text>
      <CommentUI.Actions>
        <CommentUI.Action>
          <UsersReactionList
            reactionCount={likeCount}
            isLike
            type="comment"
            postReaction={likeComment}
            postId={id}
          />
        </CommentUI.Action>
        <CommentUI.Action>
          <UsersReactionList
            reactionCount={dislikeCount}
            isLike={false}
            type="comment"
            postReaction={dislikeComment}
            postId={id}
          />
        </CommentUI.Action>
        {toggleEditComment && deleteComment && (
          <div className={styles.right}>
            <CommentUI.Action onClick={() => toggleEditComment(id)}>
              <Icon name="edit" />
              Edit
            </CommentUI.Action>
            <CommentUI.Action onClick={() => deleteComment(id)}>
              <Icon name="trash" />
              Delete
            </CommentUI.Action>
          </div>
        )}
      </CommentUI.Actions>
    </CommentUI.Content>
  </CommentUI>
);

Comment.propTypes = {
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  toggleEditComment: PropTypes.func,
  deleteComment: PropTypes.func,
  likeComment: PropTypes.func.isRequired,
  dislikeComment: PropTypes.func.isRequired
};

Comment.defaultProps = {
  toggleEditComment: undefined,
  deleteComment: undefined
};

export default Comment;
