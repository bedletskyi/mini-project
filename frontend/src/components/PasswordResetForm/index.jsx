import React, { useState } from 'react';
import PropTypes from 'prop-types';
import validator from 'validator';
import {
  Form,
  Button,
  Segment
} from 'semantic-ui-react';

const PasswordResetForm = ({ send }) => {
  const [email, setEmail] = useState('');
  const [isEmailValid, setIsEmailValid] = useState(true);
  const [isLoading, setIsLoading] = useState(false);
  const [isVisibleMessage, setIsVisibleMessage] = useState(false);

  const validateEmail = () => {
    const isValid = validator.isEmail(email);
    setIsEmailValid(isValid);
    return isValid;
  };

  const handleResetClick = async () => {
    if (!validateEmail()) {
      return;
    }
    setIsLoading(true);
    const host = window.location.origin;
    const request = { host, email };
    try {
      await send(request);
    } catch {
      setIsLoading(false);
    }
    setIsLoading(false);
    setIsVisibleMessage(true);
  };

  return isVisibleMessage
    ? (
      <p>If that account is our system, we emailed you a link to reset password</p>
    ) : (
      <Form name="resettingForm" size="large" onSubmit={handleResetClick}>
        <Segment>
          <Form.Input
            fluid
            icon="at"
            iconPosition="left"
            placeholder="Email"
            error={!isEmailValid}
            onChange={ev => setEmail(ev.target.value)}
            onBlur={validateEmail}
          />
          <Button
            type="submit"
            color={isEmailValid ? 'blue' : 'google plus'}
            fluid
            size="large"
            loading={isLoading}
          >
            Reset
          </Button>
        </Segment>
      </Form>
    );
};

PasswordResetForm.propTypes = {
  send: PropTypes.func.isRequired
};

export default PasswordResetForm;
