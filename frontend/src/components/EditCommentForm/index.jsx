import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Form, Button, Segment } from 'semantic-ui-react';

const EditCommentForm = ({
  update,
  close,
  comment
}) => {
  const [body, setBody] = useState(comment.body);

  const handleUpdateComment = async () => {
    if (!body) {
      return;
    }
    await update({ ...comment, body });
    close();
  };

  return (
    <Segment>
      <Form reply onSubmit={handleUpdateComment}>
        <Form.TextArea
          value={body}
          placeholder="Type a comment..."
          onChange={ev => setBody(ev.target.value)}
          style={{ minHeight: 200 }}
        />
        <Button type="submit" content="Update comment" labelPosition="left" icon="edit" primary />
      </Form>
    </Segment>
  );
};

EditCommentForm.propTypes = {
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  update: PropTypes.func.isRequired,
  close: PropTypes.func.isRequired
};

export default EditCommentForm;
