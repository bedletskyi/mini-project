export const getUserImgLink = image => (image
  ? image.link
  : 'https://forwardsummit.ca/wp-content/uploads/2019/01/avatar-default.png');

export const getResizedImage = async (canvas, newWidth, newHeight) => {
  const tmpCanvas = document.createElement('canvas');
  tmpCanvas.width = newWidth;
  tmpCanvas.height = newHeight;

  const ctx = tmpCanvas.getContext('2d');
  ctx.drawImage(canvas, 0, 0, canvas.width, canvas.height, 0, 0, newWidth, newHeight);

  const data = canvas.toDataURL('image/jpeg');
  const res = await fetch(data);
  const blob = await res.blob();
  return new File([blob], 'croppedImage', { type: 'image/jpeg' });
};
