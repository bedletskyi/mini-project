package com.threadjava.commentReactions.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class ResponseCommentReactionDto {
    private UUID id;
    private UUID commentId;
    private UUID authorId;
    private Boolean isLike;
    private Boolean isNewReaction;
    private UUID userId;
}
