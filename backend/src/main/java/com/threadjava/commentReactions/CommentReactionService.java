package com.threadjava.commentReactions;

import com.threadjava.comment.CommentService;
import com.threadjava.commentReactions.dto.CommentReactionDto;
import com.threadjava.commentReactions.dto.ReceivedCommentReactionDto;
import com.threadjava.commentReactions.dto.ResponseCommentReactionDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class CommentReactionService {
    @Autowired
    private CommentReactionRepository commentReactionRepository;
    @Autowired
    private CommentService commentService;

    public Optional<ResponseCommentReactionDto> setReaction(ReceivedCommentReactionDto commentReactionDto) {

        var reaction = commentReactionRepository.getCommentReaction(commentReactionDto.getUserId(),
                commentReactionDto.getCommentId());

        if (reaction.isPresent()) {
            var react = reaction.get();
            if (react.getIsLike() == commentReactionDto.getIsLike()) {
                commentReactionRepository.deleteById(react.getId());
                return Optional.empty();
            } else {
                react.setIsLike(commentReactionDto.getIsLike());
                var result = commentReactionRepository.save(react);
                return Optional.of(CommentReactionMapper.MAPPER.reactionToResponseCommentReactionDto(result));
            }
        } else {
            var commentReaction = CommentReactionMapper.MAPPER.dtoToCommentReaction(commentReactionDto);
            var result = commentReactionRepository.save(commentReaction);
            var response = CommentReactionMapper.MAPPER.reactionToResponseCommentReactionDto(result);
            response.setIsNewReaction(true);
            response.setAuthorId(commentService.getCommentById(response.getCommentId()).getUser().getId());
            return Optional.of(response);
        }
    }

    public List<CommentReactionDto> getReactions(UUID commentId) {
        return commentReactionRepository
                .getCommentReactionsByCommentId(commentId)
                .stream()
                .map(CommentReactionMapper.MAPPER::reactionToCommentReactionDto)
                .collect(Collectors.toList());
    }
}
