package com.threadjava.commentReactions;

import com.threadjava.commentReactions.dto.CommentReactionDto;
import com.threadjava.commentReactions.dto.ReceivedCommentReactionDto;
import com.threadjava.commentReactions.dto.ResponseCommentReactionDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static com.threadjava.auth.TokenService.getUserId;

@RestController
@RequestMapping("/api/commentreaction")
public class CommentReactionController {
    @Autowired
    private CommentReactionService commentReactionService;
    @Autowired
    private SimpMessagingTemplate template;

    @PutMapping
    public Optional<ResponseCommentReactionDto> setReaction(@RequestBody ReceivedCommentReactionDto commentReactionDto) {
        commentReactionDto.setUserId(getUserId());
        var reaction = commentReactionService.setReaction(commentReactionDto);

        if (reaction.isPresent() && reaction.get().getUserId() != getUserId()) {
            // notify a user if someone (not himself) liked his comment
            template.convertAndSend("/topic/commentlike", reaction);
        }
        return reaction;
    }

    @GetMapping("/{commentId}")
    public List<CommentReactionDto> getReactions(@PathVariable UUID commentId) {
        return commentReactionService.getReactions(commentId);
    }
}
