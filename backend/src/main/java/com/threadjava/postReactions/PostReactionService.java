package com.threadjava.postReactions;

import com.threadjava.mail.MailBuilder;
import com.threadjava.mail.MailSender;
import com.threadjava.post.PostsService;
import com.threadjava.postReactions.dto.PostReactionDto;
import com.threadjava.postReactions.dto.ReceivedPostReactionDto;
import com.threadjava.postReactions.dto.ResponsePostReactionDto;
import com.threadjava.users.UsersService;
import com.threadjava.users.dto.UserDetailsDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class PostReactionService {
    @Autowired
    private PostReactionsRepository postReactionsRepository;
    @Autowired
    private PostsService postsService;
    @Autowired
    private UsersService usersService;
    @Autowired
    private MailBuilder mailBuilder;
    @Autowired
    private MailSender mailSender;

    public Optional<ResponsePostReactionDto> setReaction(ReceivedPostReactionDto postReactionDto) {

        var reaction = postReactionsRepository.getPostReaction(postReactionDto.getUserId(), postReactionDto.getPostId());

        if (reaction.isPresent()) {
            var react = reaction.get();
            if (react.getIsLike() == postReactionDto.getIsLike()) {
                postReactionsRepository.deleteById(react.getId());
                return Optional.empty();
            } else {
                react.setIsLike(postReactionDto.getIsLike());
                var result = postReactionsRepository.save(react);
                return Optional.of(PostReactionMapper.MAPPER.reactionToResponsePostReactionDto(result));
            }
        } else {
            var postReaction = PostReactionMapper.MAPPER.dtoToPostReaction(postReactionDto);
            var result = postReactionsRepository.save(postReaction);
            var response = PostReactionMapper.MAPPER.reactionToResponsePostReactionDto(result);
            response.setIsNewReaction(true);
            response.setAuthorPostId(postsService.getPostById(response.getPostId()).getUser().getId());
            return Optional.of(response);
        }
    }

    public List<PostReactionDto> getReactions(UUID postId) {
        return postReactionsRepository
                .getPostReactionsByPostId(postId)
                .stream()
                .map(PostReactionMapper.MAPPER::reactionToPostReactionDto)
                .collect(Collectors.toList());
    }

    public void notifyAuthor(ResponsePostReactionDto reaction, String location) {
        if (!reaction.getIsLike()) {
            return;
        }
        String username = usersService.getUserById(reaction.getUserId()).getUsername();
        UserDetailsDto author = usersService.getUserById(
                postsService.getPostById(reaction.getPostId()).getUser().getId()
        );

        mailSender.send(mailBuilder.likePostMessage(username, reaction.getPostId(), location, author.getEmail()));
    }
}
