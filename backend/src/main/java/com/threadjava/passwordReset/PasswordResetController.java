package com.threadjava.passwordReset;

import com.threadjava.passwordReset.dto.PasswordResetCreationDto;
import com.threadjava.passwordReset.dto.PasswordResetRequestDto;
import com.threadjava.passwordReset.dto.PasswordResetTokenResponseDto;
import com.threadjava.users.dto.UserShortDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/auth/password")
public class PasswordResetController {
    @Autowired
    private PasswordResetService passwordResetService;

    @PostMapping("/reset")
    public Boolean reset(@RequestBody PasswordResetRequestDto passwordReset) {
        return passwordResetService.reset(passwordReset);
    }

    @GetMapping("/{token}")
    public PasswordResetTokenResponseDto validate(@PathVariable String token) {
        return passwordResetService.validate(token);
    }

    @PutMapping("/new")
    public UserShortDto savePassword(@RequestBody PasswordResetCreationDto password) {
        return passwordResetService.savePassword(password);
    }
}
