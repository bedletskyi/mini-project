package com.threadjava.passwordReset;

import com.threadjava.auth.TokenService;
import com.threadjava.auth.model.AuthUser;
import com.threadjava.mail.MailBuilder;
import com.threadjava.mail.MailSender;
import com.threadjava.passwordReset.dto.PasswordResetCreationDto;
import com.threadjava.passwordReset.dto.PasswordResetRequestDto;
import com.threadjava.passwordReset.dto.PasswordResetTokenResponseDto;
import com.threadjava.passwordReset.model.PasswordReset;
import com.threadjava.users.UserMapper;
import com.threadjava.users.UsersRepository;
import com.threadjava.users.dto.UserShortDto;
import com.threadjava.users.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

@Service
public class PasswordResetService {
    @Autowired
    private PasswordEncoder bCryptPasswordEncoder;
    @Autowired
    private PasswordResetRepository resetRepository;
    @Autowired
    private UsersRepository usersRepository;
    @Autowired
    private TokenService tokenService;
    @Autowired
    private MailBuilder mailBuilder;
    @Autowired
    private MailSender mailSender;

    public Boolean reset(PasswordResetRequestDto passwordReset) throws BadCredentialsException {
        Optional<User> user = usersRepository.findByEmail(passwordReset.getEmail());
        if (user.isEmpty()) {
            throw new BadCredentialsException("Incorrect email");
        }

        AuthUser authUser = new AuthUser(user.get().getId(), user.get().getPassword(), user.get().getUsername());
        String token = tokenService.generateToken(authUser);

        mailSender.send(mailBuilder.resetMessage(token, user.get(), passwordReset.getHost()));

        PasswordReset resetModel = new PasswordReset();
        resetModel.setUser(user.get());
        resetModel.setToken(token);
        resetRepository.save(resetModel);

        return true;
    }

    public PasswordResetTokenResponseDto validate(String token) {
        Optional<PasswordReset> passwordReset = resetRepository.findByToken(token);
        if (passwordReset.isEmpty() || tokenService.isTokenExpired(passwordReset.get().getToken())) {
            throw new IllegalArgumentException("Token invalid");
        }
        return PasswordResetMapper.MAPPER.passwordResetToPasswordResetTokenDto(passwordReset.get());
    }

    @Transactional
    public UserShortDto savePassword(PasswordResetCreationDto password) {
        UUID userId = UUID.fromString(tokenService.extractUserid(password.getToken()));
        Optional<User> user = usersRepository.findById(userId);
        if (user.isEmpty()) {
            throw new IllegalArgumentException("Token invalid");
        }
        user.get().setPassword(bCryptPasswordEncoder.encode(password.getPassword()));

        resetRepository.deleteByToken(password.getToken());

        return UserMapper.MAPPER.userToUserShortDto(usersRepository.save(user.get()));
    }
}
