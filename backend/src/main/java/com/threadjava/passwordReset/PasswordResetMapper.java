package com.threadjava.passwordReset;

import com.threadjava.passwordReset.dto.PasswordResetTokenResponseDto;
import com.threadjava.passwordReset.model.PasswordReset;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface PasswordResetMapper {
    PasswordResetMapper MAPPER = Mappers.getMapper(PasswordResetMapper.class);

    PasswordResetTokenResponseDto passwordResetToPasswordResetTokenDto(PasswordReset passwordReset);
}
