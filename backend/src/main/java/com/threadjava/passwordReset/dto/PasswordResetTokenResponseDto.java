package com.threadjava.passwordReset.dto;

import lombok.Data;

@Data
public class PasswordResetTokenResponseDto {
    private String token;
}
