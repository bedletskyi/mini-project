package com.threadjava.passwordReset.dto;

import lombok.Data;

@Data
public class PasswordResetCreationDto {
    private String password;
    private String token;
}
