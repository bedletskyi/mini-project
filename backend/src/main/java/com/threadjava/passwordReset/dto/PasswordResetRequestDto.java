package com.threadjava.passwordReset.dto;

import lombok.Data;

@Data
public class PasswordResetRequestDto {
    private String host;
    private String email;
}
