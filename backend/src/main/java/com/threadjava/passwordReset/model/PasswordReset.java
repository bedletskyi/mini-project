package com.threadjava.passwordReset.model;

import com.threadjava.db.BaseEntity;
import com.threadjava.users.model.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@Table(name = "password_reset")
public class PasswordReset extends BaseEntity {

    @Column(name = "token")
    private String token;

    @OneToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;
}
