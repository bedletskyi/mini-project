package com.threadjava.passwordReset;

import com.threadjava.passwordReset.model.PasswordReset;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface PasswordResetRepository extends JpaRepository<PasswordReset, UUID> {
    Optional<PasswordReset> findByToken(String token);

    void deleteByToken(String token);
}
