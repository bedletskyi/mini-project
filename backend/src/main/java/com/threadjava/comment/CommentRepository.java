package com.threadjava.comment;

import com.threadjava.comment.dto.CommentDetailsQueryResult;
import com.threadjava.comment.dto.CommentListQueryResult;
import com.threadjava.comment.model.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface CommentRepository extends JpaRepository<Comment, UUID> {
    @Query("SELECT new com.threadjava.comment.dto.CommentListQueryResult(c.id, c.body, " +
            "(SELECT COALESCE(SUM(CASE WHEN cr.isLike = TRUE THEN 1 ELSE 0 END), 0) FROM c.reactions cr WHERE cr.comment = c), " +
            "(SELECT COALESCE(SUM(CASE WHEN cr.isLike = FALSE THEN 1 ELSE 0 END), 0) FROM c.reactions cr WHERE cr.comment = c), " +
            "c.createdAt, c.user, c.post) " +
            "FROM Comment c " +
            "WHERE c.post.id = :postId")
    List<CommentListQueryResult> findAllByPostId(@Param("postId") UUID postId);

    @Query("SELECT new com.threadjava.comment.dto.CommentDetailsQueryResult(c.id, c.body, " +
            "(SELECT COALESCE(SUM(CASE WHEN cr.isLike = TRUE THEN 1 ELSE 0 END), 0) FROM c.reactions cr WHERE cr.comment = c), " +
            "(SELECT COALESCE(SUM(CASE WHEN cr.isLike = FALSE THEN 1 ELSE 0 END), 0) FROM c.reactions cr WHERE cr.comment = c), " +
            "c.user, c.post) " +
            "FROM Comment c " +
            "WHERE c.id = :id")
    Optional<CommentDetailsQueryResult> findCommentById(@Param("id") UUID id);

    @Query("UPDATE Comment c SET c.isDeleted = true WHERE c.id = :id")
    @Modifying
    void softDeleteById(@Param("id") UUID id);
}
