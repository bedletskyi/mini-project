package com.threadjava.comment;

import com.threadjava.comment.dto.CommentDetailsDto;
import com.threadjava.comment.dto.CommentSaveDto;
import com.threadjava.comment.model.Comment;
import com.threadjava.post.PostMapper;
import com.threadjava.post.PostsService;
import com.threadjava.post.dto.PostDetailsDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Service
public class CommentService {
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private PostsService postsService;

    public CommentDetailsDto getCommentById(UUID id) {
        return commentRepository.findCommentById(id)
                .map(CommentMapper.MAPPER::commentDetailsQueryResultToCommentDetailsDto)
                .orElseThrow();
    }

    public CommentDetailsDto create(CommentSaveDto commentDto) {
        var comment = CommentMapper.MAPPER.commentSaveDtoToModel(commentDto);
        var postCreated = CommentMapper.MAPPER.commentToCommentDetailsDto(commentRepository.save(comment));

        if (postCreated.getPost().getUserId() == null) {
            PostDetailsDto post = postsService.getPostById(postCreated.getPost().getId());
            postCreated.setPost(PostMapper.MAPPER.postDetailsDtoToPostShortDto(post));
        }

        return postCreated;
    }

    @Transactional
    public CommentDetailsDto update(UUID id, CommentSaveDto commentDto) {
        Comment commentFromDb = commentRepository.findById(id).orElseThrow();
        commentFromDb.setBody(commentDto.getBody());
        Comment updatedComment = commentRepository.save(commentFromDb);
        return CommentMapper.MAPPER.commentToCommentDetailsDto(updatedComment);
    }

    @Transactional
    public void delete(UUID id) {
        commentRepository.softDeleteById(id);
    }
}
