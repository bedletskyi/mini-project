package com.threadjava.comment;

import com.threadjava.comment.dto.CommentDetailsDto;
import com.threadjava.comment.dto.CommentSaveDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

import static com.threadjava.auth.TokenService.getUserId;

@RestController
@RequestMapping("/api/comments")
public class CommentController {
    @Autowired
    private CommentService commentService;
    @Autowired
    private SimpMessagingTemplate template;

    @GetMapping("/{id}")
    public CommentDetailsDto get(@PathVariable UUID id) {
        return commentService.getCommentById(id);
    }

    @PostMapping
    public CommentDetailsDto post(@RequestBody CommentSaveDto commentDto) {
        commentDto.setUserId(getUserId());
        CommentDetailsDto comment = commentService.create(commentDto);
        template.convertAndSend("/topic/new_comment", comment);
        return comment;
    }

    @PutMapping("/{id}")
    public CommentDetailsDto update(@PathVariable UUID id, @RequestBody CommentSaveDto commentDto) {
        CommentDetailsDto comment = commentService.update(id, commentDto);
        template.convertAndSend("/topic/update_comment", comment);
        return comment;
    }

    @DeleteMapping("/{id}")
    public UUID delete(@PathVariable UUID id) {
        commentService.delete(id);
        return id;
    }
}
