package com.threadjava.comment;

import com.sun.xml.bind.v2.util.StackRecorder;
import com.threadjava.comment.dto.CommentDetailsDto;
import com.threadjava.comment.dto.CommentDetailsQueryResult;
import com.threadjava.comment.dto.CommentSaveDto;
import com.threadjava.comment.model.Comment;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CommentMapper {
    CommentMapper MAPPER = Mappers.getMapper(CommentMapper.class);

    @Mapping(source = "post.user.id", target = "post.userId")
    @Mapping(target = "likeCount", ignore = true)
    @Mapping(target = "dislikeCount", ignore = true)
    CommentDetailsDto commentToCommentDetailsDto(Comment comment);

    @Mapping(source = "post.user.id", target = "post.userId")
    CommentDetailsDto commentDetailsQueryResultToCommentDetailsDto(CommentDetailsQueryResult comment);

    @Mapping(source = "postId", target = "post.id")
    @Mapping(source = "userId", target = "user.id")
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    @Mapping(target = "reactions", ignore = true)
    Comment commentSaveDtoToModel(CommentSaveDto commentDto);
}
