package com.threadjava.comment.dto;

import com.threadjava.post.dto.PostShortDto;
import com.threadjava.users.dto.UserShortDto;
import lombok.Data;

import java.util.UUID;

@Data
public class CommentDetailsDto {
    private UUID id;
    private String body;
    public long likeCount;
    public long dislikeCount;
    private UserShortDto user;
    private PostShortDto post;
}
