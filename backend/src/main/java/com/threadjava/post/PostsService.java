package com.threadjava.post;

import com.threadjava.comment.CommentRepository;
import com.threadjava.mail.MailBuilder;
import com.threadjava.mail.MailSender;
import com.threadjava.post.dto.*;
import com.threadjava.post.model.Post;
import com.threadjava.postReactions.PostReactionsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class PostsService {
    @Autowired
    private PostsRepository postsCrudRepository;
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private MailBuilder mailBuilder;
    @Autowired
    private MailSender mailSender;

    public List<PostListDto> getAllPosts(Integer from, Integer count, UUID userId) {
        var pageable = PageRequest.of(from / count, count);
        return postsCrudRepository
                .findAllPosts(userId, pageable)
                .stream()
                .map(PostMapper.MAPPER::postListToPostListDto)
                .collect(Collectors.toList());
    }

    public List<PostListDto> getAllOtherUsersPosts(Integer from, Integer count, UUID userId) {
        var pageable = PageRequest.of(from / count, count);
        return postsCrudRepository
                .findAllOtherUsersPosts(userId, pageable)
                .stream()
                .map(PostMapper.MAPPER::postListToPostListDto)
                .collect(Collectors.toList());
    }

    public List<PostListDto> getAllLikedPosts(Integer from, Integer count, UUID userId) {
        var pageable = PageRequest.of(from / count, count);
        return postsCrudRepository
                .findAllLikedPosts(userId, pageable)
                .stream()
                .map(PostMapper.MAPPER::postListToPostListDto)
                .collect(Collectors.toList());
    }

    public PostDetailsDto getPostById(UUID id) {
        var post = postsCrudRepository.findPostById(id)
                .map(PostMapper.MAPPER::postToPostDetailsDto)
                .orElseThrow();

        var comments = commentRepository.findAllByPostId(id)
                .stream()
                .map(PostMapper.MAPPER::commentToCommentDto)
                .collect(Collectors.toList());
        post.setComments(comments);

        return post;
    }

    public PostCreationResponseDto create(PostCreationDto postDto) {
        Post post = PostMapper.MAPPER.postCreationDtoToPost(postDto);
        Post postCreated = postsCrudRepository.save(post);
        return PostMapper.MAPPER.postToPostCreationResponseDto(postCreated);
    }

    @Transactional
    public PostCreationResponseDto update(UUID id, PostCreationDto postDto) {
        Post postFromDb = postsCrudRepository.findPostById(id)
                .map(PostMapper.MAPPER::postDetailsQueryToPost)
                .orElseThrow();
        Post postFromUser = PostMapper.MAPPER.postCreationDtoToPost(postDto);

        postFromDb.setBody(postFromUser.getBody());
        postFromDb.setImage(postFromUser.getImage());

        Post updatedPost = postsCrudRepository.save(postFromDb);

        return PostMapper.MAPPER.postToPostCreationResponseDto(updatedPost);
    }

    @Transactional
    public void delete(UUID id) {
        postsCrudRepository.softDeleteById(id);
    }

    public void share(PostShareDto postShareDto) {
        mailSender.send(
                mailBuilder.sharePostMessage(
                        postShareDto.getRecipientEmail(),
                        postShareDto.getUsername(),
                        postShareDto.getPostLink()
                )
        );
    }
}
