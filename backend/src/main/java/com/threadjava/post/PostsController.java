package com.threadjava.post;


import com.threadjava.post.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

import static com.threadjava.auth.TokenService.getUserId;

@RestController
@RequestMapping("/api/posts")
public class PostsController {
    @Autowired
    private PostsService postsService;
    @Autowired
    private SimpMessagingTemplate template;

    @GetMapping
    public List<PostListDto> get(@RequestParam(defaultValue = "0") Integer from,
                                 @RequestParam(defaultValue = "10") Integer count,
                                 @RequestParam(required = false) UUID userId,
                                 @RequestParam(required = false) Boolean hideMyPosts,
                                 @RequestParam(required = false) Boolean showLikedPosts) {
        if (hideMyPosts) {
            return postsService.getAllOtherUsersPosts(from, count, userId);
        } else if (showLikedPosts) {
            return postsService.getAllLikedPosts(from, count, userId);
        } else {
            return postsService.getAllPosts(from, count, userId);
        }
    }

    @GetMapping("/{id}")
    public PostDetailsDto get(@PathVariable UUID id) {
        return postsService.getPostById(id);
    }

    @PostMapping
    public PostCreationResponseDto post(@RequestBody PostCreationDto postDto) {
        postDto.setUserId(getUserId());
        var item = postsService.create(postDto);
        template.convertAndSend("/topic/new_post", item);
        return item;
    }

    @PutMapping("/{id}")
    public PostCreationResponseDto updatePost(@PathVariable UUID id, @RequestBody PostCreationDto postDto) {
        return postsService.update(id, postDto);
    }

    @DeleteMapping("/{id}")
    public UUID deletePost(@PathVariable UUID id) {
        postsService.delete(id);
        return id;
    }

    @PostMapping("/share")
    public void sharePost(@RequestBody PostShareDto postShareDto) {
        postsService.share(postShareDto);
    }
}
