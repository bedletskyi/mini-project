package com.threadjava.post.dto;

import lombok.Data;

@Data
public class PostShareDto {
    private String username;
    private String postLink;
    private String recipientEmail;
}
