package com.threadjava.post.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class PostShortDto {
    private UUID id;
    private String body;
    private UUID userId;
}
