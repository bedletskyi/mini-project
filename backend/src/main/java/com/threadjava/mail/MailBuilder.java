package com.threadjava.mail;

import com.threadjava.postReactions.dto.ResponsePostReactionDto;
import com.threadjava.users.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class MailBuilder {
    @Autowired
    private SimpleMailMessage mailMessage;

    private SimpleMailMessage build(String emailTo, String subject, String message) {
        mailMessage.setTo(emailTo);
        mailMessage.setSubject(subject);
        mailMessage.setText(message);
        return mailMessage;
    }

    public SimpleMailMessage resetMessage(String token, User user, String host) {
        String subject = "Password Reset";
        String message = String.format(
                "Hello, %s!\n" +
                        "You can now change your password. Click below.\n" +
                        "%s/password/%s",
                user.getUsername(), host, token
        );

        return build(user.getEmail(), subject, message);
    }

    public SimpleMailMessage likePostMessage(String username, UUID id, String location, String emailAuthor) {
        String subject = "Like Post";
        String message = String.format(
                "Hello!\n" +
                        "Your post was liked by %s\n" +
                        "Post: %s/share/%s",
                username, location, id
        );

        return build(emailAuthor, subject, message);
    }

    public SimpleMailMessage sharePostMessage(String recipientEmail, String username, String shareLink) {
        String subject = "Share Post";
        String message = String.format(
                "Hello!\n" +
                        "%s shared a post with you.\n" +
                        "Have a look: %s",
                username, shareLink
        );

        return build(recipientEmail, subject, message);
    }
}
