package com.threadjava.users;

import com.threadjava.users.dto.UserDetailsDto;
import com.threadjava.users.dto.UserShortDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.threadjava.auth.TokenService.getUserId;

@RestController
@RequestMapping("/api/user")
public class UserController {
    @Autowired
    private UsersService userDetailsService;

    @GetMapping
    public UserDetailsDto getUser() {
        return userDetailsService.getUserById(getUserId());
    }

    @GetMapping("/all")
    public List<UserShortDto> getAllUsers() {
        return userDetailsService.getAllUsers();
    }

    @PutMapping
    public UserDetailsDto updateUser(@RequestBody UserDetailsDto userDetailsDto) {
        return userDetailsService.updateUser(userDetailsDto);
    }
}
