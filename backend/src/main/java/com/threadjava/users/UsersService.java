package com.threadjava.users;

import com.threadjava.auth.model.AuthUser;
import com.threadjava.image.ImageMapper;
import com.threadjava.image.model.Image;
import com.threadjava.users.dto.UserDetailsDto;
import com.threadjava.users.dto.UserShortDto;
import com.threadjava.users.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class UsersService implements UserDetailsService {
    @Autowired
    private UsersRepository usersRepository;

    @Override
    public AuthUser loadUserByUsername(String email) throws UsernameNotFoundException {
        return usersRepository
                .findByEmail(email)
                .map(user -> new AuthUser(user.getId(), user.getEmail(), user.getPassword()))
                .orElseThrow(() -> new UsernameNotFoundException(email));
    }

    public UserDetailsDto getUserById(UUID id) {
        return usersRepository
                .findById(id)
                .map(UserMapper.MAPPER::userToUserDetailsDto)
                .orElseThrow(() -> new UsernameNotFoundException("No user found with username"));
    }

    public void save(User user) {
        usersRepository.save(user);
    }

    public List<UserShortDto> getAllUsers() {
        return usersRepository
                .findAll()
                .stream()
                .map(UserMapper.MAPPER::userToUserShortDto)
                .collect(Collectors.toList());
    }

    public UserDetailsDto updateUser(UserDetailsDto userDetailsDto) {
        User user = usersRepository
                .findById(userDetailsDto.getId())
                .orElseThrow();

        Image image = ImageMapper.MAPPER.imageDtoToImage(userDetailsDto.getImage());

        user.setAvatar(image);
        user.setUsername(userDetailsDto.getUsername());
        user.setStatus(userDetailsDto.getStatus());

        return UserMapper.MAPPER.userToUserDetailsDto(usersRepository.save(user));
    }
}
